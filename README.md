# My Site d2d

This module sets up migration classes and mappings for Drupal-to-Drupal migrations using the [migrate](https://drupal.org/project/migrate) and [migrate_d2d](https://drupal.org/project/migrate) modules.

The 7.x-1.x branch contains a generalized template for use as a starting point, while the other branches will include real-world examples of actual migrations.
